window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import { createApp } from 'vue';
import App from './App.vue';
import router from "./router";

axios.defaults.baseURL = '/vue-laravel-spa/public/';

const app = createApp(App);
app.use(router);
app.mount('#app');

